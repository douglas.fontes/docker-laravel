# docker-laravel

Projeto de estudos com laravel utilizando o Docker.


## Tecnologias

* (Docker)[https://www.docker.com/]
* (NGINX)[https://www.nginx.com/]
* (MYSQL)[https://www.mysql.com/]
* (PHP)[https://www.php.net/]


## Versões das tecnologias utilizadas

* Laravel em sua versão 5.8.
* Container de banco com imagem Mysql em sua versão 5.7.22.
* Container de aplicação com imgem PHP em sua versão 7.2-fpm, que vai rodar
como serviço específico, sem utilização do apache.
* Container de webserver utilizando imagem NGINX em sua versão alpine, que
é bem pequena e enxuta.

## Como levantar o ambiente

Para levantar a aplicação, execute o comando ``` $ docker-compose up -d ```.
Em seguida crie o arquivo de configuração ``` .env ``` executando
``` docker-compose exec app cp .env-example .env ``` e após feito isso,
gere a chave executando ``` docker-compose exec app php artisan key:generate ```.


## Erros na aplicação

Para corrigir erros na aplicação, execute o comando
``` $ docker-compose exec app php artisan config:cache ``` e teste novamente,
caso o erro persista, execute ``` $ docker-compose exec app composer dump ```.
